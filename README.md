# ALTA DE INVERSIONES(post) http://localhost:4000/investments/newinvestment     
 Es la ruta para enviar un alta de inversion, que deberá contener en el body un objeto con las propiedades: company, de tipo string; investedAmount, de tipo number. El monto debe ser mayor a 100.000 y multiplo de 10.000     
  Ej {company: ', investedAmount: 220000}

# MONTO INICIAL PARA INVERSIONES(get) http://localhost:4000/investments/createamount   
Es la ruta por donde se obtendrá el monto inicial, de la que se recibirá un objeto con una propiedad "amount" con la cantidad en tipo number.       
  Ej: {amount: 200000}

# TABLA INVERSIONES(get) http://localhost:4000/investments/table  
Es la ruta por la cual se obtendrán las inversiones para cargar una página de la tabla, acompañada de los parámetros page y quantity, indicando el número de la página a cargar y la cantidad de filas a mostrar por página respectivamente. Empezando por la página 0.     
  Ej: http://localhost:4000/investments/table?page=0&quantity=5
Esto devolverá un objeto con una propiedad investments(un array con las inversiones), y una propiedad totalpages(la cantidad de paginas).   
 Ej: {

    "investments": [
        {
            "_id": "5e003046f7def80d646f7fd2",
            "investedAmount": 400000,
            "company": "Porter",
            "date": "2019-12-23T03:11:02.760Z",
            "__v": 0
        },
        {
            "_id": "5e0027f19a62301cb83ff364",
            "date": "2019-12-23T02:32:02.418Z",
            "investedAmount": 100000,
            "company": "TBC",
            "__v": 0
        },
        {
            "_id": "5e001cc6e63b2f27788e1213",
            "date": "2019-12-23T01:46:35.055Z",
            "investedAmount": 220000,
            "company": "globant",
            "__v": 0
        },
        {
            "_id": "5e001e55e63b2f27788e1214",
            "date": "2019-12-23T01:46:35.055Z",
            "investedAmount": 610000,
            "company": "pepsi",
            "__v": 0
        },
        {
            "_id": "5e001e6ce63b2f27788e1215",
            "date": "2019-12-23T01:46:35.055Z",
            "investedAmount": 310000,
            "company": "coca-cola",
            "__v": 0
        }
    ],
    "totalpages": 3
}