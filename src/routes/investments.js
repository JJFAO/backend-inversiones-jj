import express from "express";
var router = express.Router();
import mongoose from "mongoose";

// Conectamos a través de mongoose el backend a la base de datos
mongoose.connect("mongodb://localhost:27017/InvestmentsDB", {
  useNewUrlParser: true
});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Conection error:"));
db.once("open", function() {});

// Creamos el esquema de datos que se va a registrar en la base de datos

const investmentSchema = new mongoose.Schema({
  company: {
    type: String,
    required: true
  },
  investedAmount: {
    type: Number,
    required: true
  },
  date: {
    type: Date,
    required: true
  }
});

// Creamos el modelo que conecta con la coleccion de nuestra base de datos,
// que tiene la estructura del esquema (estructura y tipos de datos)

const Investment = mongoose.model("Investment", investmentSchema);

// este es el metodo post con el que se agregan documentos a la coleccion

router.post("/newinvestment", (req, res, next) => {
  const investedAmount = req.body.investedAmount;

  // Validamos los datos que vienen en el req.body

  if (investedAmount < 100000 || investedAmount > 1000000) {
    res.status(501).send({error: "monto invalido, mayor a 1000000 o menor a 100000"});
    console.log("Monto invalido");
    return;
  }
  if (investedAmount % 10000 !== 0) {
    res.status(501).send({error: "monto invalido, no es multiplo de 10000"});
    console.log("Monto invalido");
    return;
  }

  // Creamos una instancia del modelo para poder enviar un nuevo documento

  const investmentData = new Investment({
    investedAmount: investedAmount,
    company: req.body.company,
    date: new Date()
  });

  // usamos el metodo save para agregar el documento a la coleccion

  investmentData.save((err, investment) => {
    if (err) {
      res.status(500).send(err);
      return;
    }

    // Devolvemos a la consulta la nueva inversion, con los datos que nos devuelve mongo

    res.send(investment);
  });
});

// Este el metodo get con el que generamos y devolvemos el monto aleatorio a invertir

router.get("/createamount", (req, res, next) => {
  // Esto sirve para generar el numero Random
  const amount = Math.floor(Math.random() * 100 + 1) * 10000;
  res.send({ amount: amount});
});

// Este el método get con el que devolvemos la documentos solicitados,
// según los parámetros página y cantidad

router.get("/table", (req, res, next) => {
  const page = parseInt(req.query.page)
  const quantity = parseInt(req.query.quantity)
  // Validamos los parametros recibidos
  if(page < 0 || isNaN(page) || quantity < 1 || isNaN(quantity)) {
    res.status(501).send({ error: "parametros invalidos" })
    return;
  }

  // Usamos el método find para traer documentos de la coleccion, los elegimos enviando
  // en el parametro options un objeto con skip y limit.
  //Agregamos el metodo sort que indica el orden que deben tener los documentos

  Investment.find({}, null, {skip: page * quantity, limit: quantity}, (err, investments) => {
    if(err) {
      res.status(500).send(err)
      return;
    }

    // Usamos el metodo estimatedDocumentCount para consultar la cantidad de documentos en la colección

    Investment.estimatedDocumentCount({}, (err, pages) => {
      if(err) {
        res.status(500).send(err)
        return;
      }

      // Devolvemos un array con los documentos solicitados y el número de páginas que hay,
      // según cuantos items se solicitan

      res.send({investments: investments, totalpages: Math.ceil(pages / quantity)})
    })

  // El método sort recibe la propiedad por la deben ordenarse los documentos,
  // en el valor se indica el sentido(ascendente o descendente)
  }).sort({date: -1})  
})

export default router;
