import express from 'express';
import mongoose from 'mongoose';

mongoose.connect('mongodb://localhost:27017/memesDB', {useNewUrlParser: true});
var router = express.Router();

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
});

var memeSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    url: String,
    thumbnailUrl: String
});

var Meme = mongoose.model('Meme', memeSchema);

let posts = [
    {
        "albumId": 1,
        "id": 1,
        "title": "accusamus beatae ad facilis cum similique qui sunt",
        "url": "https://via.placeholder.com/600/92c952",
        "thumbnailUrl": "https://i.pinimg.com/originals/a4/60/4d/a4604dd467651bb6959fda25779f232e.jpg"
    },
    {
        "albumId": 1,
        "id": 2,
        "title": "reprehenderit est deserunt velit ipsam",
        "url": "https://via.placeholder.com/600/771796",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/awoA3j1_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 3,
        "title": "officia porro iure quia iusto qui ipsa ut modi",
        "url": "https://via.placeholder.com/600/24f355",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/aZ77MRz_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 4,
        "title": "culpa odio esse rerum omnis laboriosam voluptate repudiandae",
        "url": "https://via.placeholder.com/600/d32776",
        "thumbnailUrl": "https://images3.memedroid.com/images/UPLOADED7/5a799c800b2db.jpeg"
    },
    {
        "albumId": 1,
        "id": 5,
        "title": "natus nisi omnis corporis facere molestiae rerum in",
        "url": "https://via.placeholder.com/600/f66b97",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/aDg1rD9_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 6,
        "title": "accusamus ea aliquid et amet sequi nemo",
        "url": "https://via.placeholder.com/600/56a8c2",
        "thumbnailUrl": "https://images7.memedroid.com/images/UPLOADED619/5c2c25679cd72.jpeg"
    },
    {
        "albumId": 1,
        "id": 7,
        "title": "officia delectus consequatur vero aut veniam explicabo molestias",
        "url": "https://via.placeholder.com/600/b0f7cc",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/aXjj5Yd_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 8,
        "title": "aut porro officiis laborum odit ea laudantium corporis",
        "url": "https://via.placeholder.com/600/54176f",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/an54A30_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 9,
        "title": "qui eius qui autem sed",
        "url": "https://via.placeholder.com/600/51aa97",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/ao5M0yg_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 10,
        "title": "beatae et provident et ut vel",
        "url": "https://via.placeholder.com/600/810b14",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/aBgmjXN_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 11,
        "title": "nihil at amet non hic quia qui",
        "url": "https://via.placeholder.com/600/1ee8a4",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/aQ1d0Ee_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 12,
        "title": "mollitia soluta ut rerum eos aliquam consequatur perspiciatis maiores",
        "url": "https://via.placeholder.com/600/66b7d2",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/aN0A5B4_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 13,
        "title": "repudiandae iusto deleniti rerum",
        "url": "https://via.placeholder.com/600/197d29",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/aBgnRbO_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 14,
        "title": "est necessitatibus architecto ut laborum",
        "url": "https://via.placeholder.com/600/61a65",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/avooLyb_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 15,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/ae5r78b_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 16,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/aGg12q0_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 17,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/ae5r78b_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 18,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/a3RZ1Kr_700bwp.webp"
    },
    {
        "albumId": 1,
        "id": 19,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/a7wq12r_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 20,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/ax7oMWY_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 21,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/aO0x7ZR_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 22,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/ad5P8zZ_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 23,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/aj5Vm3Q_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 24,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/aL01pY6_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 25,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/aAg0eod_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 26,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/aY7VoYv_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 27,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/a85qGOV_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 28,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/a0R4NmL_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 29,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/aqgendQ_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 30,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/aY7VyQw_460swp.webp"
    },
    {
        "albumId": 1,
        "id": 31,
        "title": "harum dicta similique quis dolore earum ex qui",
        "url": "https://via.placeholder.com/600/f9cee5",
        "thumbnailUrl": "https://img-9gag-fun.9cache.com/photo/aqgenMZ_460swp.webp"
    }
];

// localhost:4000/posts?index=3
router.get('/posts', async (req, res, next) => {

    //PRIMERA FORMA-Recibe el desde y la cantidad.
    // let indice = parseInt(req.query.index);
    // let cantidad = parseInt(req.query.quantity);
    // console.log(indice, cantidad)

    // if (indice >= 0 && cantidad >= 0) {
    //     console.log(typeof indice);

    //     let totalPages = posts.length / cantidad;
    //     // totalPages = (totalPages - parseInt(totalPages)) > 0 ? parseInt(totalPages + 1) : totalPages;
    //     totalPages = Math.ceil(totalPages);

    //     let respond = posts.slice(indice, indice + cantidad);
    //     let currentPage = Math.ceil(indice / cantidad);
    //     console.log('numero' + indice / cantidad)
    //     res.send({ result: respond, totalPages: totalPages, currentPage: currentPage });
    // } else {
    //     res.send(posts);
    // }

    let pagina = parseInt(req.query.pagina);
    let cantidad = parseInt(req.query.cantidad);
   

    if (pagina >= 0 && cantidad >= 0) {

        let desde = (pagina - 1) * cantidad;
        let hasta = desde + cantidad;
        let totalPages = posts.length / cantidad;
    

        totalPages = Math.ceil(totalPages);

         let results = posts.slice(desde, hasta);

        res.send({
            results: results,
            totalPages: totalPages,
            currentPage: pagina
        });
    } else {
        res.send(posts);
    }





});

router.get('/memes', async (req, res, next) => {
    // obtener todos los registros del Model Meme.
    Meme.find((error, memes) => {
        console.log('memes', memes);
        res.send(memes);
    });
})

router.get('/crearMeme', async (req, res, next) => {
    var meme1 = new Meme({
        title: 'Alot title',
        url: 'https://pbs.twimg.com/profile_images/1087850778/ll_400x400.jpg',
        thumbnailUrl: 'https://pbs.twimg.com/profile_images/1087850778/ll_400x400.jpg'
    });

    meme1.save((err, meme) => {
        if (err){
            res.status(500).send(err);
            return;
        }
        console.log(meme);
        res.send(meme)
    })
})

router.get('/posts/:id', async (req, res, next) => {
    var postId = req.params.id
    if (postId) {
        var post = posts.find((item) => {
            return item.id == postId
        })
        if (post) {
            res.send(post);
        } else {
            res.status(404).send('page not found')
        }
    } else {
        res.send(posts);
    }
})


export default router;
